package nstu.geo.presentation.controller.tab;


import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Line;

public class ModelController {

    @FXML private VBox modelSpace;

    @FXML
    public void initialize() {

        Canvas canvas = new Canvas(1200, 800);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        drawShapes(gc);
        modelSpace.getChildren().add(canvas);
    }



    private void drawShapes(GraphicsContext gc) {
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);
        gc.strokeLine(0, 400, 2200, 400);
        gc.strokeLine(600, 0, 600, 800);
        gc.setStroke(Color.BLUE);
        gc.strokeRect(100, 300, 1000, 100);
    }
}
