package nstu.geo.segy;

import nstu.geo.core.application.command.Command;
import nstu.geo.core.application.command.SimpleCommandBus;
import nstu.geo.core.application.command.ValidationCommandBus;
import nstu.geo.core.application.command.segy.ConvertSegyToIeeeFormatCommand;
import nstu.geo.core.domain.model.segy.SegyFile;
import nstu.geo.core.domain.model.segy.SegyFileFabric;
import nstu.geo.core.infrastructure.service.segy.converter.SegyFloatConverter;
import nstu.geo.core.infrastructure.service.spline.SegySplineInterpolator;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class Test {

    public static void main(String[] args) throws IOException {

        Map<Integer, String> map = new HashMap<Integer, String>(); ;
        map.put(1, "data/ieee5.seg");
        map.put(2, "data/ibm1.sgy");

        File file = new File(map.get(1));

        SegyFile segyFile = SegyFileFabric.createNew(file);
        byte[] trace = segyFile.getSeismicTraceBytes(1,0,1000);

        int[] x = new int[trace.length / 4];
        float[] y = new float[trace.length / 4];
        SegyFloatConverter floatConverter = new SegyFloatConverter(segyFile);
        int index = 0;
        for (int k = 0; k < trace.length;) {
            byte[] fByte = new byte[]{trace[k], trace[k + 1], trace[k + 2], trace[k + 3]};
            x[index] = index;
            y[index] = floatConverter.convert(fByte);
            k += 4;
            index++;
        }
        System.out.println(y[1]);
        System.out.println(y[2]);
        System.out.println(y[3]);
        System.out.println(y[4]);
        System.out.println(y[5]);
        System.out.println("...............");
        SegySplineInterpolator interpolator = new SegySplineInterpolator();
        interpolator.interpolate(x, y);
        for(float i = 1; i < 1000;) {
            System.out.println(i + " " + interpolator.value(i));
            i += 0.1;
        }
    }


}
